module gitlab.com/akita/mem

require (
	github.com/golang/mock v1.3.1
	github.com/onsi/ginkgo v1.10.2
	github.com/onsi/gomega v1.5.0
	github.com/rs/xid v1.2.1
	gitlab.com/akita/akita v1.9.0
	gitlab.com/akita/util v0.2.0
	go.mongodb.org/mongo-driver v1.1.0 // indirect
)

go 1.13
