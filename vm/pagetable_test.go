package vm

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("PageTable", func() {

	var (
		pageTable *PageTableImpl
		page      *Page
	)

	BeforeEach(func() {
		page = &Page{
			PID:      1,
			PAddr:    0x0,
			VAddr:    0x1000,
			PageSize: 4096,
			Valid:    true,
		}
		pageTable = DefaultPageTableFactory{
			Log2PageSize: 12,
		}.Build().(*PageTableImpl)
	})

	It("should insert a page", func() {
		pageTable.InsertPage(page)
		Expect(pageTable.entries.Len()).To(Equal(1))
		Expect(pageTable.entriesTable[0x1000].Value).To(Equal(page))
	})

	It("should panic when inserting a page that is already exist", func() {
		pageTable.InsertPage(page)
		Expect(func() {
			pageTable.InsertPage(page)
		}).To(Panic())
	})

	It("should find page", func() {
		pageTable.InsertPage(page)
		retPage := pageTable.FindPage(0x1000)
		Expect(retPage).To(BeIdenticalTo(page))
	})

	It("should find page if address is not aligned", func() {
		pageTable.InsertPage(page)
		retPage := pageTable.FindPage(0x1024)
		Expect(retPage).To(BeIdenticalTo(page))
	})

	It("should remove page", func() {
		page1 := page
		pageTable.InsertPage(page1)
		page2 := &Page{PID: 2, VAddr: 0x2000, PageSize: 4096, Valid: true}
		pageTable.InsertPage(page2)

		pageTable.RemovePage(0x2000)

		Expect(pageTable.entriesTable).NotTo(ContainElement(0x2000))
		Expect(pageTable.entries.Len()).To(Equal(1))

	})

	It("should return nil if page is not found", func() {
		pageTable.InsertPage(page)
		retPage := pageTable.FindPage(0x2000)
		Expect(retPage).To(BeNil())
	})
})
