package vm

import (
	"container/list"
	"log"
	"sync"

	"gitlab.com/akita/util/ca"
)

// A Page is an entry in the page table, maintaining the information about how
// to translate a virtual address to a physical address
type Page struct {
	PID         ca.PID
	PAddr       uint64
	VAddr       uint64
	PageSize    uint64
	Valid       bool
	GPUID       uint64
	Unified     bool
	IsMigrating bool
	IsPinned    bool
}

// A PageTable holds the meta-data of the pages.
type PageTable interface {
	InsertPage(page *Page)
	RemovePage(vAddr uint64)
	FindPage(vAddr uint64) *Page
	GetNumPages() int
	GetAllPages() *list.List
}

// PageTableImpl is the default implementation of a Page Table
type PageTableImpl struct {
	sync.Mutex
	log2PageSize uint64
	entries      *list.List
	entriesTable map[uint64]*list.Element
}

// InsertPage put a new page into the PageTable
func (pt *PageTableImpl) InsertPage(page *Page) {
	p := pt.FindPage(page.VAddr)
	if p != nil {
		log.Panic("Page already exists")
	}

	pt.Lock()
	elem := pt.entries.PushBack(page)
	pt.entriesTable[page.VAddr] = elem
	pt.Unlock()
}

//GetNumPages returns the number of page table entries
func (pt *PageTableImpl) GetNumPages() int {
	return len(pt.entriesTable)
}

// GetAllPages returns a list that hold all the pages
func (pt *PageTableImpl) GetAllPages() *list.List {
	return pt.entries
}

// RemovePage removes the entry in the page table that contains the target
// address.
func (pt *PageTableImpl) RemovePage(vAddr uint64) {
	pt.Lock()

	elem := pt.entriesTable[vAddr]
	pt.entries.Remove(elem)
	delete(pt.entriesTable, vAddr)

	pt.Unlock()
}

// FindPage returns the page that contains the given virtual address. This
// function returns nil if the page is not found.
func (pt *PageTableImpl) FindPage(vAddr uint64) *Page {
	pt.Lock()
	defer pt.Unlock()

	vAddr = (vAddr >> pt.log2PageSize) << pt.log2PageSize
	elem := pt.entriesTable[vAddr]
	if elem == nil {
		return nil
	}

	return elem.Value.(*Page)
}

// A PageTableFactory builds PageTables
type PageTableFactory interface {
	Build() PageTable
}

// DefaultPageTableFactory builds PageTableImpl
type DefaultPageTableFactory struct {
	Log2PageSize uint64
}

// Build returns a default page table implementation
func (f DefaultPageTableFactory) Build() PageTable {
	return &PageTableImpl{
		log2PageSize: f.Log2PageSize,
		entries:      list.New(),
		entriesTable: make(map[uint64]*list.Element),
	}
}
