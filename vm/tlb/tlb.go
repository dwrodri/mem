package tlb

import (
	"log"
	"reflect"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem/vm"
	"gitlab.com/akita/util/tracing"
)

type set struct {
	Blocks   []vm.Page
	LRUQueue []int
}

// A TLB is a cache that maintains some page information.
type TLB struct {
	*akita.TickingComponent

	TopPort     akita.Port
	BottomPort  akita.Port
	ControlPort akita.Port

	LowModule akita.Port

	numSets        int
	numWays        int
	pageSize       uint64
	numReqPerCycle int

	Sets []*set

	mshr mshr

	toRspToTop []*vm.TranslationRsp

	isPaused bool
}

// Reset sets all the entries int he TLB to be invalid
func (tlb *TLB) reset() {
	tlb.Sets = make([]*set, tlb.numSets)
	for i := 0; i < tlb.numSets; i++ {
		set := &set{}
		tlb.Sets[i] = set
		set.Blocks = make([]vm.Page, tlb.numWays)
		set.LRUQueue = make([]int, tlb.numWays)
		for j := 0; j < tlb.numWays; j++ {
			set.LRUQueue[j] = j
		}
	}
}

func (tlb *TLB) respondToTop(now akita.VTimeInSec) bool {
	if len(tlb.toRspToTop) == 0 {
		return false
	}
	rsp := tlb.toRspToTop[0]
	err := tlb.TopPort.Send(rsp)

	if err == nil {
		tlb.toRspToTop = tlb.toRspToTop[1:]
		return true
	}
	return false
}

// Tick defines how TLB update states at each cycle
func (tlb *TLB) Tick(now akita.VTimeInSec) bool {
	madeProgress := false

	madeProgress = tlb.performCtrlReq(now) || madeProgress

	if !tlb.isPaused {
		for i := 0; i < tlb.numReqPerCycle; i++ {
			madeProgress = tlb.parseBottom(now) || madeProgress
		}

		for i := 0; i < tlb.numReqPerCycle; i++ {
			madeProgress = tlb.lookup(now) || madeProgress
		}

		for i := 0; i < tlb.numReqPerCycle; i++ {
			madeProgress = tlb.respondToTop(now) || madeProgress
		}
	}

	return madeProgress
}

func (tlb *TLB) lookup(now akita.VTimeInSec) bool {
	msg := tlb.TopPort.Peek()
	if msg == nil {
		return false
	}

	req := msg.(*vm.TranslationReq)

	mshrEntry := tlb.mshr.Query(req.PID, req.VAddr)
	if mshrEntry != nil {
		ok := tlb.processTLBMSHRHit(now, mshrEntry, req)
		if ok {
			tlb.TopPort.Retrieve(now)
			return true
		}
		return false
	}

	for i, set := range tlb.Sets {
		for j, block := range set.Blocks {
			if tlb.isHit(block, req) {
				rsp := vm.TranslationRspBuilder{}.
					WithSendTime(now).
					WithSrc(tlb.TopPort).
					WithDst(req.Src).
					WithRspTo(req.ID).
					WithPage(block).
					Build()

				tlb.toRspToTop = append(tlb.toRspToTop, rsp)

				tlb.visit(i, j)
				tlb.TopPort.Retrieve(now)

				tracing.TraceReqReceive(req, now, tlb)
				tracing.TraceReqComplete(req, now, tlb)

				return true
			}
		}
	}

	if tlb.mshr.IsFull() {
		return false
	}

	fetched := tlb.fetchBottom(now, req)
	if fetched {
		tlb.TopPort.Retrieve(now)
		return true
	}

	return false
}

func (tlb *TLB) processTLBMSHRHit(
	now akita.VTimeInSec,
	mshrEntry *mshrEntry,
	req *vm.TranslationReq,
) bool {
	mshrEntry.Requests = append(mshrEntry.Requests, req)
	return true
}

func (tlb *TLB) fetchBottom(now akita.VTimeInSec, req *vm.TranslationReq) bool {
	fetchBottom := vm.TranslationReqBuilder{}.
		WithSendTime(now).
		WithSrc(tlb.BottomPort).
		WithDst(tlb.LowModule).
		WithPID(req.PID).
		WithVAddr(req.VAddr).
		WithGPUID(req.GPUID).
		Build()
	err := tlb.BottomPort.Send(fetchBottom)
	if err != nil {
		return false
	}

	mshrEntry := tlb.mshr.Add(req.PID, req.VAddr)
	mshrEntry.Requests = append(mshrEntry.Requests, req)

	tracing.TraceReqReceive(req, now, tlb)
	tracing.TraceReqInitiate(fetchBottom, now, tlb,
		tracing.MsgIDAtReceiver(req, tlb))

	return true
}

func (tlb *TLB) parseBottom(now akita.VTimeInSec) bool {
	item := tlb.BottomPort.Peek()
	if item == nil {
		return false
	}

	rsp := item.(*vm.TranslationRsp)
	page := rsp.Page

	mshrEntryPresent := tlb.mshr.IsEntryPresent(rsp.Page.PID, rsp.Page.VAddr)

	if mshrEntryPresent {
		mshrEntry := tlb.mshr.GetEntry(rsp.Page.PID, rsp.Page.VAddr)
		for i := 0; i < len(mshrEntry.Requests); i++ {
			req := mshrEntry.Requests[i]
			rspToTop := vm.TranslationRspBuilder{}.
				WithSendTime(now).
				WithSrc(tlb.TopPort).
				WithDst(req.Src).
				WithRspTo(req.ID).
				WithPage(page).
				Build()

			rspToTop.Page = page
			tlb.toRspToTop = append(tlb.toRspToTop, rspToTop)

			tracing.TraceReqFinalize(req, now, tlb)
			tracing.TraceReqComplete(req, now, tlb)
		}
	} else {
		tlb.BottomPort.Retrieve(now)
		return true
	}

	tlb.mshr.Remove(rsp.Page.PID, rsp.Page.VAddr)

	setID := int(page.VAddr / tlb.pageSize % uint64(tlb.numSets))
	set := tlb.Sets[setID]
	wayID := set.LRUQueue[0]
	set.Blocks[wayID] = page
	tlb.visit(setID, wayID)
	tlb.BottomPort.Retrieve(now)

	return true
}

func (tlb *TLB) performCtrlReq(now akita.VTimeInSec) bool {
	item := tlb.ControlPort.Peek()
	if item == nil {
		return false
	}

	item = tlb.ControlPort.Retrieve(now)

	switch req := item.(type) {
	case *TLBFlushReq:
		return tlb.handleTLBFlush(now, req)
	case *TLBRestartReq:
		return tlb.handleTLBRestart(now, req)
	default:
		log.Panicf("cannot process request %s", reflect.TypeOf(req))
	}

	return true
}

func (tlb *TLB) isHit(block vm.Page, req *vm.TranslationReq) bool {
	if !block.Valid {
		return false
	}

	if block.PID != req.PID {
		return false
	}

	if block.VAddr != req.VAddr {
		return false
	}

	return true
}

func (tlb *TLB) visit(setID, wayID int) {
	set := tlb.Sets[setID]
	for i, b := range set.LRUQueue {
		if b == wayID {
			set.LRUQueue = append(set.LRUQueue[:i], set.LRUQueue[i+1:]...)
			set.LRUQueue = append(set.LRUQueue, b)
			return
		}
	}
}

func (tlb *TLB) handleTLBFlush(now akita.VTimeInSec, req *TLBFlushReq) bool {
	rsp := TLBFlushRspBuilder{}.
		WithSrc(tlb.ControlPort).
		WithDst(req.Src).
		WithSendTime(now).
		Build()

	err := tlb.ControlPort.Send(rsp)
	if err != nil {
		log.Panicf("Unable to send shootdown complete rsp")
	}

	for _, set := range tlb.Sets {
		for wayID, block := range set.Blocks {
			if !block.Valid || block.PID != req.PID {
				continue
			}

			for _, vAddr := range req.VAddr {
				if block.VAddr == vAddr {
					set.Blocks[wayID].Valid = false
				}
			}
		}
	}

	tlb.mshr.Reset()
	tlb.isPaused = true
	return true
}

func (tlb *TLB) handleTLBRestart(now akita.VTimeInSec, req *TLBRestartReq) bool {
	tlb.isPaused = false

	for tlb.TopPort.Retrieve(now) != nil {
		tlb.TopPort.Retrieve(now)
	}

	for tlb.BottomPort.Retrieve(now) != nil {
		tlb.BottomPort.Retrieve(now)
	}

	rsp := TLBRestartRspBuilder{}.
		WithSendTime(now).
		WithSrc(tlb.ControlPort).
		WithDst(req.Src).
		Build()

	err := tlb.ControlPort.Send(rsp)

	if err != nil {
		log.Panicf("Unable to send restart rsp to TLB")
	}

	return true
}
