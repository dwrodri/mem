package writeback

import (
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/cache"
	"gitlab.com/akita/util"
	"gitlab.com/akita/util/akitaext"
)

type cacheState int

const (
	cacheStateInvalid cacheState = iota
	cacheStateRunning
	cacheStatePreFlushing
	cacheStateFlushing
	cacheStatePaused
)

// A Cache in writeback package  is a cache that performs the write-back policy
type Cache struct {
	*akita.TickingComponent

	TopPort     akita.Port
	BottomPort  akita.Port
	ControlPort akita.Port

	dirStageBuffer  util.Buffer
	bankBuffers     []util.Buffer
	mshrStageBuffer util.Buffer
	flusherBuffer   util.Buffer

	topParser    *topParser
	bottomParser *bottomParser
	dirStage     *directoryStage
	bankStages   []*bankStage
	mshrStage    *mshrStage
	flusher      *flusher

	storage         *mem.Storage
	lowModuleFinder cache.LowModuleFinder
	log2BlockSize   uint64
	directory       cache.Directory
	mshr            cache.MSHR

	topSender         akitaext.BufferedSender
	bottomSender      akitaext.BufferedSender
	controlPortSender akitaext.BufferedSender

	state                cacheState
	inFlightTransactions []*transaction
	pendingEvictions     []*transaction
}

func (c *Cache) Tick(now akita.VTimeInSec) bool {
	madeProgress := false

	madeProgress = c.controlPortSender.Tick(now) || madeProgress

	if c.state != cacheStatePaused {
		madeProgress = c.runPipeline(now) || madeProgress
	}

	madeProgress = c.flusher.Tick(now) || madeProgress

	return madeProgress
}

func (c *Cache) runPipeline(now akita.VTimeInSec) bool {
	madeProgress := false

	madeProgress = c.topSender.Tick(now) || madeProgress
	madeProgress = c.bottomSender.Tick(now) || madeProgress
	madeProgress = c.mshrStage.Tick(now) || madeProgress
	for _, bs := range c.bankStages {
		madeProgress = bs.Tick(now) || madeProgress
	}
	madeProgress = c.bottomParser.Tick(now) || madeProgress
	madeProgress = c.dirStage.Tick(now) || madeProgress

	madeProgress = c.topParser.Tick(now) || madeProgress

	return madeProgress
}

func (c *Cache) discardInflightTransactions(now akita.VTimeInSec) {
	sets := c.directory.GetSets()
	for _, set := range sets {
		for _, block := range set.Blocks {
			block.ReadCount = 0
			block.IsLocked = false
		}
	}

	c.dirStage.Reset(now)
	for _, bs := range c.bankStages {
		bs.Reset(now)
	}
	c.mshrStage.Reset(now)

	clearPort(c.TopPort, now)
	clearPort(c.BottomPort, now)

	c.mshr.Reset()
	c.flushBuffer(c.flusherBuffer)

	c.topSender.Clear()
	c.bottomSender.Clear()

	c.inFlightTransactions = nil
	c.pendingEvictions = nil
}

func (c *Cache) flushBuffer(buffer util.Buffer) {
	for buffer.Pop() != nil {
	}
}

// NewWriteBackCache creates a new write-through cache,
// injecting the dependency of the engine, the directory and the storage.
func NewWriteBackCache(
	name string,
	engine akita.Engine,
	directory cache.Directory,
	mshr cache.MSHR,
	lowModuleFinder cache.LowModuleFinder,
	storage *mem.Storage,
) *Cache {
	cache := new(Cache)
	cache.TickingComponent = akita.NewTickingComponent(
		name, engine, 1*akita.GHz, cache)

	defaultLog2BlockSize := uint64(6)
	cache.log2BlockSize = defaultLog2BlockSize

	cache.TopPort = akita.NewLimitNumMsgPort(cache, 4, name+".ToTop")
	cache.BottomPort = akita.NewLimitNumMsgPort(cache, 4, name+".BottomPort")
	cache.ControlPort = akita.NewLimitNumMsgPort(cache, 4, name+".ControlPort")

	cache.dirStageBuffer = util.NewBuffer(1)
	cache.bankBuffers = make([]util.Buffer, 1)
	cache.bankBuffers[0] = util.NewBuffer(1)
	cache.mshrStageBuffer = util.NewBuffer(1)
	cache.flusherBuffer = util.NewBuffer(1)

	cache.topSender = akitaext.NewBufferedSender(
		cache.TopPort, util.NewBuffer(4))
	cache.bottomSender = akitaext.NewBufferedSender(
		cache.BottomPort, util.NewBuffer(4))
	cache.controlPortSender = akitaext.NewBufferedSender(
		cache.ControlPort, util.NewBuffer(4))

	cache.topParser = &topParser{cache: cache}
	cache.bottomParser = &bottomParser{cache: cache}
	cache.dirStage = &directoryStage{cache: cache}
	cache.bankStages = make([]*bankStage, 1)
	cache.bankStages[0] = &bankStage{
		cache:   cache,
		bankID:  0,
		latency: 1,
	}
	cache.mshrStage = &mshrStage{cache: cache}
	cache.flusher = &flusher{cache: cache}

	cache.directory = directory
	cache.mshr = mshr
	cache.storage = storage
	cache.lowModuleFinder = lowModuleFinder
	cache.state = cacheStateRunning

	return cache
}
