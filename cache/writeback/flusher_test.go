package writeback

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/cache"
	"gitlab.com/akita/util"
)

var _ = Describe("Flusher", func() {
	var (
		mockCtrl          *gomock.Controller
		controlPort       *MockPort
		topPort           *MockPort
		bottomPort        *MockPort
		directory         *MockDirectory
		dirBuf            *MockBuffer
		bankBuf           *MockBuffer
		mshrStageBuf      *MockBuffer
		flusherBuf        *MockBuffer
		topPortSender     *MockBufferedSender
		bottomPortSender  *MockBufferedSender
		controlPortSender *MockBufferedSender
		mshr              *MockMSHR
		cacheModule       *Cache
		f                 *flusher
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		controlPort = NewMockPort(mockCtrl)
		topPort = NewMockPort(mockCtrl)
		bottomPort = NewMockPort(mockCtrl)
		directory = NewMockDirectory(mockCtrl)
		directory.EXPECT().WayAssociativity().Return(2).AnyTimes()
		dirBuf = NewMockBuffer(mockCtrl)
		bankBuf = NewMockBuffer(mockCtrl)
		mshrStageBuf = NewMockBuffer(mockCtrl)
		flusherBuf = NewMockBuffer(mockCtrl)
		topPortSender = NewMockBufferedSender(mockCtrl)
		bottomPortSender = NewMockBufferedSender(mockCtrl)
		controlPortSender = NewMockBufferedSender(mockCtrl)
		mshr = NewMockMSHR(mockCtrl)

		builder := Builder{
			WayAssociativity: 4,
			BlockSize:        64,
			ByteSize:         6,
		}

		cacheModule = builder.Build()
		cacheModule.TopPort = topPort
		cacheModule.BottomPort = bottomPort
		cacheModule.ControlPort = controlPort
		cacheModule.directory = directory
		cacheModule.mshr = mshr
		cacheModule.dirStageBuffer = dirBuf
		cacheModule.bankBuffers = []util.Buffer{bankBuf}
		cacheModule.mshrStageBuffer = mshrStageBuf
		cacheModule.topSender = topPortSender
		cacheModule.bottomSender = bottomPortSender
		cacheModule.controlPortSender = controlPortSender
		cacheModule.flusherBuffer = flusherBuf
		cacheModule.dirStage = &directoryStage{cache: cacheModule}
		cacheModule.mshrStage = &mshrStage{cache: cacheModule}

		f = &flusher{cache: cacheModule}
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should do nothing if no request", func() {
		controlPort.EXPECT().Peek().Return(nil)
		ret := f.Tick(10)
		Expect(ret).To(BeFalse())
	})

	Context("flush without reset", func() {
		It("should start flushing", func() {
			req := cache.FlushReqBuilder{}.WithSendTime(8).Build()
			controlPort.EXPECT().Peek().Return(req)
			controlPort.EXPECT().Retrieve(gomock.Any())

			ret := f.Tick(10)

			Expect(ret).To(BeTrue())
			Expect(f.processingFlush).To(BeIdenticalTo(req))
			Expect(cacheModule.state).To(Equal(cacheStatePreFlushing))
		})

		It("should do nothing if there is inflight transaction", func() {
			cacheModule.state = cacheStatePreFlushing
			cacheModule.inFlightTransactions = append(
				cacheModule.inFlightTransactions, &transaction{})
			req := cache.FlushReqBuilder{}.WithSendTime(8).Build()
			f.processingFlush = req

			ret := f.Tick(10)

			Expect(ret).To(BeFalse())
		})

		It("should move to flush stage if no inflight transaction", func() {
			cacheModule.state = cacheStatePreFlushing
			cacheModule.inFlightTransactions = nil
			req := cache.FlushReqBuilder{}.WithSendTime(8).Build()
			f.processingFlush = req

			sets := []cache.Set{
				{Blocks: []*cache.Block{
					{IsDirty: true, IsValid: true},
					{IsDirty: false, IsValid: true},
				}},
				{Blocks: []*cache.Block{
					{IsDirty: true, IsValid: false},
					{IsDirty: false, IsValid: false},
				}},
			}
			directory.EXPECT().GetSets().Return(sets)

			ret := f.Tick(10)

			Expect(ret).To(BeTrue())
			Expect(cacheModule.state).To(Equal(cacheStateFlushing))
			Expect(f.blockToEvict).To(HaveLen(1))
			Expect(f.evictingCount).To(Equal(1))
		})

		It("should do nothing if no block to evict", func() {
			cacheModule.state = cacheStateFlushing
			req := cache.FlushReqBuilder{}.WithSendTime(8).Build()
			f.processingFlush = req
			f.evictingCount = 10
			flusherBuf.EXPECT().Pop().Return(nil)

			f.blockToEvict = []*cache.Block{}

			ret := f.Tick(10)

			Expect(ret).To(BeFalse())
		})

		It("should stall if bank buffer is full", func() {
			cacheModule.state = cacheStateFlushing
			req := cache.FlushReqBuilder{}.WithSendTime(8).Build()
			f.processingFlush = req
			f.evictingCount = 10
			flusherBuf.EXPECT().Pop().Return(nil)

			blocks := []*cache.Block{{Tag: 0x0}, {Tag: 0x40}}
			f.blockToEvict = []*cache.Block{blocks[0], blocks[1]}

			bankBuf.EXPECT().CanPush().Return(false)

			ret := f.Tick(10)

			Expect(ret).To(BeFalse())
		})

		It("should send read for eviction to bank", func() {
			cacheModule.state = cacheStateFlushing
			req := cache.FlushReqBuilder{}.WithSendTime(8).Build()
			f.processingFlush = req
			flusherBuf.EXPECT().Pop().Return(nil)

			blocks := []*cache.Block{{Tag: 0x0}, {Tag: 0x40}}
			f.blockToEvict = []*cache.Block{blocks[0], blocks[1]}
			f.evictingCount = 2

			bankBuf.EXPECT().CanPush().Return(true)
			bankBuf.EXPECT().Push(gomock.Any()).Do(func(trans *transaction) {
				Expect(trans.bankAction).To(Equal(bankReadForEviction))
			})

			ret := f.Tick(10)

			Expect(ret).To(BeTrue())
			Expect(f.blockToEvict).NotTo(ContainElement(blocks[0]))
			Expect(f.blockToEvict).To(ContainElement(blocks[1]))
		})

		It("should extract from flusherBuf", func() {
			cacheModule.state = cacheStateFlushing
			req := cache.FlushReqBuilder{}.WithSendTime(8).Build()
			f.processingFlush = req
			f.blockToEvict = []*cache.Block{}

			flusherBuf.EXPECT().Pop().Return(&mem.WriteDoneRsp{})

			ret := f.Tick(10)

			Expect(ret).To(BeTrue())
			Expect(f.returnedEviction).To(Equal(1))
		})

		It("should stall is controlPort sender is busy", func() {
			cacheModule.state = cacheStateFlushing
			req := cache.FlushReqBuilder{}.WithSendTime(8).Build()
			f.processingFlush = req
			f.blockToEvict = []*cache.Block{}
			f.evictingCount = 10
			f.returnedEviction = 10

			flusherBuf.EXPECT().Pop().Return(nil)

			controlPortSender.EXPECT().CanSend(1).Return(false)

			ret := f.Tick(10)

			Expect(ret).To(BeFalse())
		})

		It("should send response if all the blocks are evicted", func() {
			cacheModule.state = cacheStateFlushing
			req := cache.FlushReqBuilder{}.WithSendTime(8).Build()
			f.processingFlush = req
			f.blockToEvict = []*cache.Block{}
			f.evictingCount = 10
			f.returnedEviction = 9

			flusherBuf.EXPECT().Pop().Return(&mem.WriteDoneRsp{})
			directory.EXPECT().Reset()
			controlPortSender.EXPECT().CanSend(1).Return(true)
			controlPortSender.EXPECT().Send(gomock.Any()).
				Do(func(rsp *cache.FlushRsp) {
					Expect(rsp.RspTo).To(Equal(req.ID))
				})

			ret := f.Tick(10)

			Expect(ret).To(BeTrue())
			Expect(f.processingFlush).To(BeNil())
			Expect(f.returnedEviction).To(Equal(0))
			Expect(f.evictingCount).To(Equal(0))
			Expect(cacheModule.state).To(Equal(cacheStateRunning))
		})
	})

	Context("flush with reset", func() {
		It("should remove inflight state", func() {
			req := cache.FlushReqBuilder{}.
				WithSendTime(8).
				DiscardInflight().
				Build()
			sets := []cache.Set{
				{Blocks: []*cache.Block{
					{IsDirty: true, IsValid: true, IsLocked: true},
					{IsDirty: false, IsValid: true},
				}},
				{Blocks: []*cache.Block{
					{IsDirty: true, IsValid: false},
					{IsDirty: false, IsValid: false},
				}},
			}

			controlPort.EXPECT().Peek().Return(req)
			controlPort.EXPECT().Retrieve(gomock.Any())
			directory.EXPECT().GetSets().Return(sets)
			mshr.EXPECT().Reset()
			bankBuf.EXPECT().Pop().Return(nil)
			dirBuf.EXPECT().Pop().Return(nil)
			flusherBuf.EXPECT().Pop().Return(nil)
			mshrStageBuf.EXPECT().Pop().Return(nil)
			topPort.EXPECT().Retrieve(gomock.Any()).Return(nil)
			bottomPort.EXPECT().Retrieve(gomock.Any()).Return(nil)
			topPortSender.EXPECT().Clear()
			bottomPortSender.EXPECT().Clear()

			ret := f.Tick(10)

			Expect(ret).To(BeTrue())
			Expect(f.processingFlush).To(BeIdenticalTo(req))
			Expect(cacheModule.state).To(Equal(cacheStatePreFlushing))
			Expect(sets[0].Blocks[0].IsLocked).To(BeFalse())
		})
	})

	Context("flush with pause", func() {
		It("should send response if all the blocks are evicted", func() {
			cacheModule.state = cacheStateFlushing
			req := cache.FlushReqBuilder{}.
				WithSendTime(8).
				PauseAfterFlushing().
				Build()
			f.processingFlush = req
			f.blockToEvict = []*cache.Block{}
			f.evictingCount = 10
			f.returnedEviction = 9

			flusherBuf.EXPECT().Pop().Return(&mem.WriteDoneRsp{})
			directory.EXPECT().Reset()
			controlPortSender.EXPECT().CanSend(1).Return(true)
			controlPortSender.EXPECT().Send(gomock.Any()).
				Do(func(rsp *cache.FlushRsp) {
					Expect(rsp.RspTo).To(Equal(req.ID))
				})

			ret := f.Tick(10)

			Expect(ret).To(BeTrue())
			Expect(f.processingFlush).To(BeNil())
			Expect(f.returnedEviction).To(Equal(0))
			Expect(f.evictingCount).To(Equal(0))
			Expect(cacheModule.state).To(Equal(cacheStatePaused))
		})
	})

	Context("restarting", func() {
		It("should stall if cannot send to control port", func() {
			req := cache.RestartReqBuilder{}.WithSendTime(10).Build()
			controlPort.EXPECT().Peek().Return(req)
			controlPortSender.EXPECT().CanSend(1).Return(false)

			madeProgress := f.Tick(10)

			Expect(madeProgress).To(BeFalse())
		})

		It("should restart", func() {
			req := cache.RestartReqBuilder{}.WithSendTime(10).Build()
			controlPort.EXPECT().Peek().Return(req)
			controlPort.EXPECT().Retrieve(gomock.Any())
			controlPortSender.EXPECT().Send(gomock.Any())
			controlPortSender.EXPECT().CanSend(1).Return(true)
			topPort.EXPECT().Retrieve(gomock.Any()).Return(nil)
			bottomPort.EXPECT().Retrieve(gomock.Any()).Return(nil)

			madeProgress := f.Tick(10)

			Expect(madeProgress).To(BeTrue())
			Expect(cacheModule.state).To(Equal(cacheStateRunning))
		})
	})
})
