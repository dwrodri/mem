package writeback

import (
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/cache"
)

type bankAction int

const (
	bankActionInvalid bankAction = iota
	bankReadHit
	bankWriteHit
	bankReadForEviction
	bankWriteFetched
)

type transaction struct {
	read         *mem.ReadReq
	write        *mem.WriteReq
	block        *cache.Block
	victim       *cache.Block
	evictingAddr uint64
	eviction     *mem.WriteReq
	evictionDone *mem.WriteDoneRsp
	mshrEntry    *cache.MSHREntry
	bankAction   bankAction
}

func (t transaction) req() mem.AccessReq {
	if t.read != nil {
		return t.read
	}
	if t.write != nil {
		return t.write
	}
	return nil
}
