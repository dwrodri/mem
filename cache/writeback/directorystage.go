package writeback

import (
	"fmt"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/cache"
	"gitlab.com/akita/util/ca"
	"gitlab.com/akita/util/tracing"
)

type directoryStage struct {
	cache *Cache
}

func (ds *directoryStage) Tick(now akita.VTimeInSec) bool {
	item := ds.cache.dirStageBuffer.Peek()
	if item == nil {
		return false
	}

	trans := item.(*transaction)
	if trans.read != nil {
		return ds.doRead(now, trans)
	}
	return ds.doWrite(now, trans)
}

func (ds *directoryStage) Reset(now akita.VTimeInSec) {
	clearBuffer(ds.cache.dirStageBuffer)
}

func (ds *directoryStage) doRead(
	now akita.VTimeInSec,
	trans *transaction,
) bool {
	cachelineID, _ := getCacheLineID(
		trans.read.Address, ds.cache.log2BlockSize)
	block := ds.cache.directory.Lookup(
		trans.read.PID, cachelineID)
	if block != nil {
		return ds.handleReadHit(now, trans, block)
	}
	return ds.handleReadMiss(now, trans)
}

func (ds *directoryStage) handleReadHit(
	now akita.VTimeInSec,
	trans *transaction,
	block *cache.Block,
) bool {
	if block.IsLocked {
		return false
	}

	tracing.AddTaskStep(
		tracing.MsgIDAtReceiver(trans.read, ds.cache),
		now, ds.cache,
		"r-hit",
	)

	return ds.readFromBank(trans, block)
}

func (ds *directoryStage) handleReadMiss(
	now akita.VTimeInSec,
	trans *transaction,
) bool {
	req := trans.read
	cacheLineID, _ := getCacheLineID(req.Address, ds.cache.log2BlockSize)

	mshrEntry := ds.cache.mshr.Query(req.PID, cacheLineID)

	if mshrEntry != nil {
		trans.mshrEntry = mshrEntry
		mshrEntry.Requests = append(mshrEntry.Requests, trans)
		ds.cache.dirStageBuffer.Pop()

		tracing.AddTaskStep(
			tracing.MsgIDAtReceiver(trans.read, ds.cache),
			now, ds.cache,
			"r-miss-mshr-hit",
		)
		return true
	}

	if ds.cache.mshr.IsFull() {
		return false
	}

	victim := ds.cache.directory.FindVictim(cacheLineID)
	if victim.IsLocked || victim.ReadCount > 0 {
		return false
	}

	if ds.needEviction(victim) {
		tracing.AddTaskStep(
			tracing.MsgIDAtReceiver(trans.read, ds.cache),
			now, ds.cache,
			"r-miss-mshr-miss-need-eviction",
		)
		return ds.evict(now, trans, victim)
	}

	tracing.AddTaskStep(
		tracing.MsgIDAtReceiver(trans.read, ds.cache),
		now, ds.cache,
		"r-miss-mshr-miss-no-eviction",
	)
	return ds.fetch(now, trans, victim)
}

func (ds *directoryStage) doWrite(
	now akita.VTimeInSec,
	trans *transaction,
) bool {
	write := trans.write
	cachelineID, _ := getCacheLineID(write.Address, ds.cache.log2BlockSize)
	block := ds.cache.directory.Lookup(trans.write.PID, cachelineID)

	if block != nil {
		return ds.doWriteHit(trans, block)
	}
	return ds.doWriteMiss(now, trans)
}

func (ds *directoryStage) doWriteHit(
	trans *transaction,
	block *cache.Block,
) bool {
	if block.IsLocked || block.ReadCount > 0 {
		return false
	}
	return ds.writeToBank(trans, block)
}

func (ds *directoryStage) doWriteMiss(
	now akita.VTimeInSec,
	trans *transaction,
) bool {
	write := trans.write
	cachelineID, _ := getCacheLineID(write.Address, ds.cache.log2BlockSize)

	mshrEntry := ds.cache.mshr.Query(write.PID, cachelineID)
	if mshrEntry != nil {
		trans.mshrEntry = mshrEntry
		mshrEntry.Requests = append(mshrEntry.Requests, trans)
		ds.cache.dirStageBuffer.Pop()
		return true
	}

	if ds.isWritingFullLine(write) {
		return ds.writeFullLineMiss(now, trans)
	}
	return ds.writePartialLineMiss(now, trans)
}

func (ds *directoryStage) writeFullLineMiss(now akita.VTimeInSec, trans *transaction) bool {
	write := trans.write
	cachelineID, _ := getCacheLineID(write.Address, ds.cache.log2BlockSize)

	victim := ds.cache.directory.FindVictim(cachelineID)
	if victim.IsLocked || victim.ReadCount > 0 {
		return false
	}

	if ds.needEviction(victim) {
		return ds.evict(now, trans, victim)
	}

	return ds.writeToBank(trans, victim)
}

func (ds *directoryStage) writePartialLineMiss(
	now akita.VTimeInSec,
	trans *transaction,
) bool {
	write := trans.write
	cachelineID, _ := getCacheLineID(write.Address, ds.cache.log2BlockSize)

	if ds.cache.mshr.IsFull() {
		return false
	}

	victim := ds.cache.directory.FindVictim(cachelineID)
	if victim.IsLocked || victim.ReadCount > 0 {
		return false
	}

	if ds.needEviction(victim) {
		return ds.evict(now, trans, victim)
	}

	return ds.fetch(now, trans, victim)
}

func (ds *directoryStage) readFromBank(
	trans *transaction,
	block *cache.Block,
) bool {
	numBanks := len(ds.cache.bankBuffers)
	bank := bankID(block, ds.cache.directory.WayAssociativity(), numBanks)
	bankBuf := ds.cache.bankBuffers[bank]

	if !bankBuf.CanPush() {
		return false
	}

	ds.cache.directory.Visit(block)
	block.ReadCount++
	trans.block = block
	trans.bankAction = bankReadHit
	ds.cache.dirStageBuffer.Pop()
	bankBuf.Push(trans)
	return true
}

func (ds *directoryStage) writeToBank(
	trans *transaction,
	block *cache.Block,
) bool {
	numBanks := len(ds.cache.bankBuffers)
	bank := bankID(block, ds.cache.directory.WayAssociativity(), numBanks)
	bankBuf := ds.cache.bankBuffers[bank]

	if !bankBuf.CanPush() {
		return false
	}

	addr := trans.write.Address
	cachelineID, _ := getCacheLineID(addr, ds.cache.log2BlockSize)

	ds.cache.directory.Visit(block)
	block.IsLocked = true
	block.Tag = cachelineID
	block.IsValid = true
	block.PID = trans.write.PID
	trans.block = block
	trans.bankAction = bankWriteHit
	ds.cache.dirStageBuffer.Pop()
	bankBuf.Push(trans)
	return true
}

func (ds *directoryStage) evict(
	now akita.VTimeInSec,
	trans *transaction,
	victim *cache.Block,
) bool {
	bankNum := bankID(victim,
		ds.cache.directory.WayAssociativity(), len(ds.cache.bankBuffers))
	bankBuf := ds.cache.bankBuffers[bankNum]

	if !bankBuf.CanPush() {
		return false
	}

	var addr uint64
	var pid ca.PID
	if trans.read != nil {
		addr = trans.read.Address
		pid = trans.read.PID
	} else {
		addr = trans.write.Address
		pid = trans.write.PID
	}

	cacheLineID, _ := getCacheLineID(addr, ds.cache.log2BlockSize)

	if !(trans.write != nil && ds.isWritingFullLine(trans.write)) {
		mshrEntry := ds.cache.mshr.Add(pid, cacheLineID)
		mshrEntry.Block = victim
		mshrEntry.Requests = append(mshrEntry.Requests, trans)
		trans.mshrEntry = mshrEntry
	}

	trans.bankAction = bankReadForEviction
	trans.victim = &cache.Block{
		Tag:          victim.Tag,
		CacheAddress: victim.CacheAddress,
		DirtyMask:    victim.DirtyMask,
	}
	trans.block = victim

	victim.Tag = cacheLineID
	victim.PID = pid
	victim.IsLocked = true
	victim.IsDirty = false

	ds.cache.dirStageBuffer.Pop()
	bankBuf.Push(trans)

	trans.evictingAddr = trans.victim.Tag
	ds.cache.pendingEvictions = append(ds.cache.pendingEvictions, trans)

	return true
}

func (ds *directoryStage) fetch(
	now akita.VTimeInSec,
	trans *transaction,
	block *cache.Block,
) bool {
	var addr uint64
	var pid ca.PID
	var req mem.AccessReq
	if trans.read != nil {
		req = trans.read
		addr = trans.read.Address
		pid = trans.read.PID
	} else {
		req = trans.write
		addr = trans.write.Address
		pid = trans.write.PID
	}
	cacheLineID, _ := getCacheLineID(addr, ds.cache.log2BlockSize)

	if ds.isEvicting(cacheLineID) {
		return false
	}

	if !ds.cache.bottomSender.CanSend(1) {
		return false
	}

	mshrEntry := ds.cache.mshr.Add(pid, cacheLineID)
	trans.mshrEntry = mshrEntry
	trans.block = block
	block.IsLocked = true
	block.Tag = cacheLineID
	block.PID = pid
	block.IsValid = true
	ds.cache.directory.Visit(block)

	tracing.AddTaskStep(
		tracing.MsgIDAtReceiver(req, ds.cache),
		now, ds.cache,
		fmt.Sprintf("add-mshr-entry-0x%x-0x%x", mshrEntry.Address, block.Tag),
	)

	ds.cache.dirStageBuffer.Pop()

	dst := ds.cache.lowModuleFinder.Find(cacheLineID)
	read := mem.ReadReqBuilder{}.
		WithSendTime(now).
		WithSrc(ds.cache.BottomPort).
		WithDst(dst).
		WithAddress(cacheLineID).
		WithByteSize(1 << ds.cache.log2BlockSize).
		Build()
	ds.cache.bottomSender.Send(read)
	tracing.TraceReqInitiate(read, now, ds.cache,
		tracing.MsgIDAtReceiver(req, ds.cache))

	mshrEntry.ReadReq = read
	mshrEntry.Requests = append(mshrEntry.Requests, trans)
	mshrEntry.Block = block

	return true
}

func (ds *directoryStage) isEvicting(addr uint64) bool {
	for _, t := range ds.cache.pendingEvictions {
		if t.evictingAddr == addr {
			return true
		}
	}
	return false
}

func (ds *directoryStage) isWritingFullLine(write *mem.WriteReq) bool {
	if len(write.Data) != (1 << ds.cache.log2BlockSize) {
		return false
	}

	if write.DirtyMask != nil {
		for _, dirty := range write.DirtyMask {
			if !dirty {
				return false
			}
		}
	}

	return true
}

func (ds *directoryStage) needEviction(victim *cache.Block) bool {
	return victim.IsValid && victim.IsDirty
}
