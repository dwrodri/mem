package writeback

import (
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/util/tracing"
)

type bankStage struct {
	cache   *Cache
	bankID  int
	latency int

	cycleLeft    int
	currentTrans *transaction
}

func (s *bankStage) Tick(now akita.VTimeInSec) bool {
	if s.currentTrans != nil {
		s.cycleLeft--
		if s.cycleLeft < 0 {
			return s.finalizeTrans(now)
		}
		return true
	}
	return s.pullFromBuf()
}

func (s *bankStage) Reset(now akita.VTimeInSec) {
	clearBuffer(s.cache.bankBuffers[s.bankID])
	s.currentTrans = nil
}

func (s *bankStage) pullFromBuf() bool {
	inBuf := s.cache.bankBuffers[s.bankID]
	trans := inBuf.Pop()
	if trans == nil {
		return false
	}
	s.cycleLeft = s.latency
	s.currentTrans = trans.(*transaction)
	return true
}

func (s *bankStage) finalizeTrans(now akita.VTimeInSec) bool {
	switch s.currentTrans.bankAction {
	case bankReadHit:
		return s.finalizeReadHit(now)
	case bankWriteHit:
		return s.finalizeWriteHit(now)
	case bankWriteFetched:
		return s.finalizeBankWriteFetched(now)
	case bankReadForEviction:
		return s.finalizeBankReadForEviction(now)
	default:
		panic("bank action not supported")
	}
}

func (s *bankStage) finalizeReadHit(now akita.VTimeInSec) bool {
	if !s.cache.topSender.CanSend(1) {
		s.cycleLeft = 0
		return false
	}

	read := s.currentTrans.read
	addr := read.Address
	_, offset := getCacheLineID(addr, s.cache.log2BlockSize)
	block := s.currentTrans.block

	data, err := s.cache.storage.Read(
		block.CacheAddress+offset, read.AccessByteSize)
	if err != nil {
		panic(err)
	}

	s.removeTransaction(s.currentTrans)

	s.currentTrans = nil
	block.ReadCount--

	dataReady := mem.DataReadyRspBuilder{}.
		WithSendTime(now).
		WithSrc(s.cache.TopPort).
		WithDst(read.Src).
		WithRspTo(read.ID).
		WithData(data).
		Build()

	s.cache.topSender.Send(dataReady)

	tracing.TraceReqComplete(read, now, s.cache)

	return true
}

func (s *bankStage) finalizeWriteHit(now akita.VTimeInSec) bool {
	if !s.cache.topSender.CanSend(1) {
		s.cycleLeft = 0
		return false
	}

	write := s.currentTrans.write
	addr := write.Address
	_, offset := getCacheLineID(addr, s.cache.log2BlockSize)
	block := s.currentTrans.block

	data, err := s.cache.storage.Read(
		block.CacheAddress, 1<<s.cache.log2BlockSize)
	if err != nil {
		panic(err)
	}
	dirtyMask := block.DirtyMask
	if dirtyMask == nil {
		dirtyMask = make([]bool, 1<<s.cache.log2BlockSize)
	}
	for i := 0; i < len(write.Data); i++ {
		if write.DirtyMask == nil || write.DirtyMask[i] {
			index := offset + uint64(i)
			data[index] = write.Data[i]
			dirtyMask[index] = true
		}
	}
	err = s.cache.storage.Write(block.CacheAddress, data)
	if err != nil {
		panic(err)
	}

	block.IsValid = true
	block.IsLocked = false
	block.IsDirty = true
	block.DirtyMask = dirtyMask

	s.removeTransaction(s.currentTrans)

	s.currentTrans = nil

	done := mem.WriteDoneRspBuilder{}.
		WithSendTime(now).
		WithSrc(s.cache.TopPort).
		WithDst(write.Src).
		WithRspTo(write.ID).
		Build()
	s.cache.topSender.Send(done)

	tracing.TraceReqComplete(write, now, s.cache)

	return true
}

func (s *bankStage) finalizeBankWriteFetched(now akita.VTimeInSec) bool {
	if !s.cache.mshrStageBuffer.CanPush() {
		s.cycleLeft = 0
		return false
	}

	mshrEntry := s.currentTrans.mshrEntry
	block := mshrEntry.Block
	s.cache.mshrStageBuffer.Push(mshrEntry)
	s.cache.storage.Write(block.CacheAddress, mshrEntry.Data)
	block.IsLocked = false
	block.IsValid = true

	s.currentTrans = nil

	return true
}

func (s *bankStage) removeTransaction(trans *transaction) {
	for i, t := range s.cache.inFlightTransactions {
		if trans == t {
			s.cache.inFlightTransactions = append(
				(s.cache.inFlightTransactions)[:i],
				(s.cache.inFlightTransactions)[i+1:]...)
			return
		}
	}
	panic("transaction not found")
}

func (s *bankStage) finalizeBankReadForEviction(now akita.VTimeInSec) bool {
	if !s.cache.bottomSender.CanSend(1) {
		s.cycleLeft = 0
		return false
	}

	trans := s.currentTrans
	victim := trans.victim
	dst := s.cache.lowModuleFinder.Find(victim.Tag)
	data, err := s.cache.storage.Read(
		victim.CacheAddress, 1<<s.cache.log2BlockSize)
	if err != nil {
		panic(err)
	}

	write := mem.WriteReqBuilder{}.
		WithSendTime(now).
		WithSrc(s.cache.BottomPort).
		WithDst(dst).
		WithAddress(victim.Tag).
		WithData(data).
		WithDirtyMask(victim.DirtyMask).
		Build()
	trans.eviction = write
	s.cache.bottomSender.Send(write)

	s.currentTrans = nil

	req := trans.req()
	if req != nil {
		tracing.TraceReqInitiate(write, now, s.cache,
			tracing.MsgIDAtReceiver(req, s.cache))
	}

	return true
}
